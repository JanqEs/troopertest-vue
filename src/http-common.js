import axios from 'axios';

export default axios.create({
  baseURL: 'https://troopertest.herokuapp.com/api/',
});
