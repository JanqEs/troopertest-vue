import http from '../http-common';

class UserDataService {
  getAll() {
    return http.get('/user', {
      headers: {
        'Content-type': 'application/json',
        'X-Auth-Token': sessionStorage.getItem('token'),
      },
    });
  }

  get(id) {
    return http.get(`/user/${id}`, {
      headers: {
        'Content-type': 'application/json',
      },
    });
  }

  register(data) {
    return http.post('/user', data);
  }

  login(data) {
    return http.post(`/user/login`, data);
  }

  findByFirstName(firstName) {
    return http.get(`/user/?firstName=${firstName}`, {
      headers: {
        'Content-type': 'application/json',
        'X-Auth-Token': sessionStorage.getItem('token'),
      },
    });
  }
}

export default new UserDataService();
