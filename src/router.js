import { createWebHistory, createRouter } from 'vue-router';

const routes = [
  {
    path: '/',
    name: 'login',
    component: () => import('./components/Login'),
  },
  {
    path: '/',
    alias: '/users',
    name: 'users',
    component: () => import('./components/UsersList'),
  },
  {
    path: '/register',
    name: 'register',
    component: () => import('./components/Register'),
  },
];

const router = createRouter({
  history: createWebHistory(),
  routes,
});

export default router;
